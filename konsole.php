<?php
require_once('connect_mysql.php');

?>

<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Konsole</title>
    <link rel="stylesheet" href="style.css"> 
</head>
<body>
<form>
        <h1 name="konsole">Konsole</h1>
        <hr>
        <main>
            <table>
            <tr><th colspan="2">Eigenschaften</th></tr>
            <?php
            $result = $connection->query("SELECT * FROM konsole");
            while ($product = $result->fetch_assoc()) {
            ?>
                <tr>
                    <?php
                    foreach ($product as $key => $value) {
                    ?>
                        <td><?php echo $value; ?></td>
                    <?php
                    }
                    ?>
                </tr>
            <?php
            }
            ?>
             <tr>
                    <td><a href = "index.php">zurück</a></td>
                </tr>

            </table>
        </main>
    </form>
</body>
</html>